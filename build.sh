#!/bin/sh
FLAGS="-Wall -Wextra -fsanitize=address,undefined -Iinclude"
# FLAGS="-Wall -Wextra -Iinclude"
LIBS="-lm -lraylib"
OUT="build"

# gcc $FLAGS -g -c -o $OUT/mat_lib.o mat_lib.c
gcc $FLAGS -g -o $OUT/gol main.c $LIBS
