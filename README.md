# A small conway's game of life made in C using raylib.

To switch between editing and running press space. Red grid indicate editing. When in editing mode, press left button to toggle a cell.
This was mainly made for education purposes.
