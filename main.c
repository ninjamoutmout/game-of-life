#include <math.h>
#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

typedef struct Dir{
    int x, y;
} Dir;

const Dir dirs[8] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};

int surround_sum(int *board, int x, int y){
    int acc =0;
    for (int i = 0; i < 8; i++){
        int dx = dirs[i].x;
        int dy = dirs[i].y;
        int cx = (((x +dx) % 100) + 100) % 100;
        int cy = (((y +dy) % 100) + 100) % 100;
        acc += board[cx * 100 + cy];
    }
    return acc;
}

void tick(int *board){
    int * temp_board = (int *)malloc(sizeof(int) * 100 * 100);
    assert(temp_board != NULL);
    memcpy(temp_board, board, sizeof(int) * 100 * 100);
    for (int i = 0; i <  100; i++){
        for (int j = 0; j < 100; j++){
            int sum = surround_sum(temp_board, i, j);
            if (temp_board[i * 100 + j] == 1){
                if (sum != 3 && sum != 2){
                    board[i * 100 + j] = 0;
                }
            }
            else if (temp_board[i * 100 + j] == 0) {
                if (sum == 3) board[i * 100 + j] = 1;
            }
        }
    }
    free(temp_board);
}

void draw(int * board, int cell_size){
    for (int i = 0; i <  100; i++){
        for (int j = 0; j < 100; j++){
            if (board[i * 100 + j] == 1){
                DrawRectangle(i * 10, j * 10, 10, 10, BLACK);
            } else {
                DrawRectangle(i * 10, j * 10, 10, 10, WHITE);
            }
        }
    }
}

void draw_grid(int mode, int width, int height){
    for (int i = 0; i < 100; i++){
        if (mode == 0){
            DrawLine(i * 10, 0, i * 10, height, GRAY);
            DrawLine(0, i * 10, width, i * 10, GRAY);
        } else {
            DrawLine(i * 10, 0, i * 10, height, RED);
            DrawLine(0, i * 10, width, i * 10, RED);
        }
    }
}

int main(void){
    int width = 1000;
    int height = 1000;
    int cell_size = width / 100;

    int mode = 1; // mode 0 = running , 1 = editing

    int *board = (int *)malloc(sizeof(int) * 100 * 100);
    if (board == NULL){ return 1; }
    for (int i = 0; i <  100; i++){
        for (int j = 0; j < 100; j++){
            board[i * 100 + j] = 0;
        }
    }

    InitWindow(width, height, "game of life");
    SetTargetFPS(120);

    while (!WindowShouldClose()) {
        ClearBackground(RAYWHITE);

        if (IsKeyPressed(KEY_SPACE)){
            mode = (mode + 1) % 2;
        }

        BeginDrawing();
        draw(board, cell_size);
        draw_grid(mode, width, height);
        if (mode == 0){
            tick(board);
            usleep(83000);
        } else {
            if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
                int x = floor(GetMouseX()) / 10;
                int y = floor(GetMouseY()) / 10;
                int c = board[x * 100 + y];
                board[x * 100 + y] = (c+1)%2;
            }
        }
        EndDrawing();
    }
    CloseWindow();
    free(board);
}


